/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab5.Student;

/**
 *
 * @author grant
 */
public class Student {
    private int id;
    private String name;
    
    public Student(){
        
    }
     public Student(int id, String name){
       this .name=name;
       this .id=id;
    }
     
     public void setName(String name){
        this .name=name; 
     }
     public String getName(){
        return name; 
     }
     public void setid(int id){
        this .id=id; 
     }
     
     public int getId(){
        return id; 
     }
          
}
