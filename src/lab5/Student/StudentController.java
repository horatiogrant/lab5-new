/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab5.Student;

/**
 *
 * @author grant
 */
public class StudentController {
    private Student model;
    private StudentView view;
    
    public StudentController(Student model,StudentView view ){
        this.model=model;
        this.view=view;
        
    }
    
    public void setName(String name){
        model.setName(name);
    }
    public String getName(){
        return model.getName();
    }
    
     public void setId(int id){
        model.setid(id);
    }
    public int getId(){
        return model.getId();
    }
    
    public void update(){
        view.print(model.getName(),model.getId());
    }
 
    
}
